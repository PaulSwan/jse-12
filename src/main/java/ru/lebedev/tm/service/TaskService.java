package ru.lebedev.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.lebedev.tm.entity.Task;
import ru.lebedev.tm.repository.TaskRepository;

import java.util.List;
import java.util.Objects;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name) {
        if(StringUtils.isEmpty(name)) return null;
        return taskRepository.create(name);
    }

    public Task create(final String name, final String description) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(description)) return null;
        return taskRepository.create(name, description);
    }

    public Task create(final String name, final String description, final Long userId) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(description) || userId == null) return null;
        return taskRepository.create(name, description, userId);
    }

    public Task update(Long id, String name, String description) {
        if(id == null || StringUtils.isEmpty(name) || StringUtils.isEmpty(description)) return null;
        return taskRepository.update(id, name, description);
    }

    public void clear() {
        taskRepository.clear();
    }

    public Task findByIndex(int index) {
        if (index < 0 || index > taskRepository.numberOfTasks() - 1) return null;
        return taskRepository.findByIndex(index);
    }

    public Task findByName(String name) {
        if (StringUtils.isEmpty(name)) return null;
        return taskRepository.findByName(name);
    }

    public Task removeById(Long id) {
        if(id == null) return null;
        return taskRepository.removeById(id);
    }

    public Task removeByName(String name) {
        if(StringUtils.isEmpty(name)) return null;
        return taskRepository.removeByName(name);
    }

    public Task removeByIndex(int index) {
        if (index < 0 || index > taskRepository.numberOfTasks() - 1) return null;
        return taskRepository.removeByIndex(index);
    }

    public Task findById(Long id) {
        if(id == null) return null;
        return taskRepository.findById(id);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public List<Task> findAll(final Long userId) {
        if (userId == null) return null;
        return taskRepository.findAll(userId);
    }

    public List<Task> findAllByProjectId(Long projectId) {
        if(projectId == null) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task findByProjectIdAndId(Long projectId, Long id) {
        if(projectId == null  || id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }
}