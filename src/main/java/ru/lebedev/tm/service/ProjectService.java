package ru.lebedev.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.lebedev.tm.entity.Project;
import ru.lebedev.tm.repository.ProjectRepository;

import java.util.List;
import java.util.Objects;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(String name) {
        if(StringUtils.isEmpty(name)) return null;
        return projectRepository.create(name);

    }

    public Project create(final String name, final String description) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(description)) return null;
        return projectRepository.create(name, description);
    }

    public Project create(final String name,final String description, final Long userId) {
        if(StringUtils.isEmpty(name) || StringUtils.isEmpty(description) || userId == null)
        return null;

        return projectRepository.create(name, description, userId);
    }

    public Project update(Long id, String name, String description) {
        if(id == null) return null;
        if(StringUtils.isEmpty(name)) return null;
        if(StringUtils.isEmpty(description)) return null;
        return projectRepository.update(id, name, description);
    }

    public void clear() {
        projectRepository.clear();
    }

    public Project findByIndex(int index) {
        if (index < 0 || index > projectRepository.numberOfProjects() - 1) return null;
        return projectRepository.findByIndex(index);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAll(final Long userId) {
        return projectRepository.findAll(userId);
    }

    public Project findByName(String name) {
        if(StringUtils.isEmpty(name)) return null;
        return projectRepository.findByName(name);
    }

    public Project findById(Long id) {
        if(id == null) return null;
        return projectRepository.findById(id);
    }

    public Project removeByIndex(int index) {
        if (index < 0 || index > projectRepository.numberOfProjects() - 1) return null;
        return projectRepository.removeByIndex(index);
    }

    public Project removeById(Long id) {
        if(id == null) return null;
        return projectRepository.removeById(id);
    }

    public Project removeByName(String name) {
        if (StringUtils.isEmpty(name)) return null;
        return projectRepository.removeByName(name);
    }

 }
