package ru.lebedev.tm.repository;

import ru.lebedev.tm.entity.Project;
import ru.lebedev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectRepository {

    private List<Project> projects = new ArrayList<>();
    private List<Project> userProjects = new ArrayList<>();

    public Project create(final String name) {
        //final Project project = new Project(name);
        //projects.add(project);
        //return project;
        final Project project = create(name, null);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projects.add(project);
        return project;
    }

    public Project create(final String name, final String description, final Long userId) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projects.add(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (Objects.isNull(project)) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;

    }

    public void clear() {
        projects.clear();
    }

    public int numberOfProjects() {
        return projects.size();
    }

    public Project findByIndex(final int index) {
        return projects.get(index);
    }

    public List<Project> findAll() {
        return projects;
    }

    public List<Project> findAll(final Long userId) {
        if (!Objects.isNull(userProjects) || !userProjects.isEmpty()) userProjects.clear();
        for (final Project project: projects) {
            if(userId.equals(project.getUserId()))
                userProjects.add(project);
        }
        return userProjects;
    }

    public Project findByName(final String name) {
        for (final Project project: projects) {
            if(project.getName().equals(name)) return project;
        }
        return null;

    }

    public Project findById(final Long id) {
       for (final Project project: projects) {
            if(project.getId().equals(id)) return project;
        }
        return null;

    }

    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (Objects.isNull(project)) return null;
        projects.remove(project);
        return project;

    }

    public Project removeById(final Long id) {
        final Project project = findById(id);
        if (Objects.isNull(project)) return null;
        projects.remove(project);
        return project;

    }

    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (Objects.isNull(project)) return null;
        projects.remove(project);
        return project;

    }
    public Project findByUserIdAndId(final Long userId, final Long projectId) {
        if (Objects.isNull(projectId) || Objects.isNull(userId)) return null;
        for (final Project project: projects) {
            final Long idUser = project.getUserId();
            if(Objects.isNull(idUser)) continue;
            if(!idUser.equals(userId)) continue;
            if(project.getId().equals(projectId)) return project;
        }
        return null;
    }


}
